import time
from datetime import datetime

print("Starting job: ", datetime.now())
print("Sleeping for 10 seconds...")
time.sleep(10)
print("Ending job: ", datetime.now())
