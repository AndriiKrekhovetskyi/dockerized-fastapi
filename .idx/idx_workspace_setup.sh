#!/bin/bash

# Colors for better readability.
BBLACK='\n\033[1;30m' # Bold Black.
BBLUE='\033[1;34m'    # Bold Blue.
BLUE='\033[0;34m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BYELLOW='\033[1;33m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color.
NC='\033[0m' # No Color.

PRIVATE_SSH_KEY_FILE="gitlab_ssh"
PUBLIC_SSH_KEY_FILE="gitlab_ssh.pub"
SSH_CONFIG_FILE="config"
SSH_DIR=~/.ssh
WAITING_TIME=3 # To have time to read messages.

echo -e "${BBLUE}Welcome to the IDX environment setup!${NC}"
echo -e "${BBLACK}Step 1: Add your private SSH key${NC}"
echo "If you need help generating an SSH key or finding it, please head over to the page below and follow the step 3:"
echo -e "${BLUE}https://gitlab.com/codeavors/traveltech/backend/shared/-/blob/main/docs/wiki/how_to.md?ref_type=heads#clone-all-your-repositories${NC}"
echo -e "Please open the file ${BLUE}'./$PRIVATE_SSH_KEY_FILE'${NC} in the project root directory and paste your private SSH key into it."
echo -e "Then, please open the file ${BLUE}'./$PUBLIC_SSH_KEY_FILE'${NC} in the project root directory and paste your public SSH key into it."
echo -e "${BYELLOW}NOTE:${NC}${YELLOW} Make sure that both files end with a blank line.${NC}"
echo "After pasting the keys, save files, and type 'y' to continue."

touch ./$PRIVATE_SSH_KEY_FILE
touch ./$PUBLIC_SSH_KEY_FILE

while true; do
    read -rp "Have you pasted your SSH keys into the files? (y/n): " CONFIRMATION
    if [ "$CONFIRMATION" = "y" ]; then
        echo -e "${GREEN}That's great! Let's continue.${NC}"
        break
    else
        echo -e "${RED}Please make sure to paste the SSH keys into the files and try again.${NC}"
    fi
done

echo -e "${BBLACK}Step 2: Creating a '$SSH_DIR' folder and files${NC}"
mkdir --parents $SSH_DIR
mv $PRIVATE_SSH_KEY_FILE $SSH_DIR/$PRIVATE_SSH_KEY_FILE
mv $PUBLIC_SSH_KEY_FILE $SSH_DIR/$PUBLIC_SSH_KEY_FILE
chmod 600 $SSH_DIR/$PRIVATE_SSH_KEY_FILE
touch $SSH_DIR/$SSH_CONFIG_FILE
chmod 600 $SSH_DIR/$SSH_CONFIG_FILE
cat <<EOF >>$SSH_DIR/$SSH_CONFIG_FILE

# GITLAB
Host gitlab gitlab.com
    HostName gitlab.com
    PreferredAuthentications publickey
    IdentityFile $SSH_DIR/$PRIVATE_SSH_KEY_FILE
    IdentitiesOnly yes
    AddKeysToAgent yes
EOF
sleep $WAITING_TIME

echo -e "${BBLACK}Step 3: Adding the key to your SSH agent${NC}"
eval "$(ssh-agent -s)"
ssh-add $SSH_DIR/$PRIVATE_SSH_KEY_FILE
sleep $WAITING_TIME

echo -e "${BBLACK}Step 4: Checking if the key is valid${NC}"
if ! ssh -T git@gitlab.com; then
    echo -e "${RED}Error: SSH key is not valid. Please check your key and try again${NC}"
    echo -e "${RED}Removing '$SSH_DIR' folder and exiting${NC}"
    rm -r $SSH_DIR
    exit 1
fi
ssh -T git@gitlab.com
echo -e "${GREEN}SSH key added successfully!${NC}"
sleep $WAITING_TIME

echo -e "${BBLACK}Step 5: Git setup${NC}"
read -rp "Please enter your Git username: " GIT_USERNAME
read -rp "Please enter your Git email: " GIT_EMAIL

git remote set-url origin git@gitlab.com:codeavors/traveltech/backend/"${PWD##*/}".git
git config --global user.name "$GIT_USERNAME"
git config --global user.email "$GIT_EMAIL"
git config --global init.defaultBranch main
git config --global pull.rebase true
git config diff.lfs.textconv cat
git config pull.ff true
git-lfs install
git-lfs pull
echo -e "${GREEN}Git configured successfully!${NC}"
sleep $WAITING_TIME

# shellcheck disable=SC2046
export $(grep -v '^#' .env | xargs)
if [[ $IDX_USE_NIX_ENVIRONMENT != "true" || -d ".venv" ]]; then
    exit
fi
echo -e "${BBLACK}Step 6: Setting up the Nix environment${NC}"
# Poetry.
export POETRY_VIRTUALENVS_CREATE=true
# Make poetry create the virtual environment in the project's root (it gets named `.venv`).
export POETRY_VIRTUALENVS_IN_PROJECT=true
export POETRY_VIRTUALENVS_OPTIONS_NO_PIP=true
export POETRY_VIRTUALENVS_OPTIONS_NO_SETUPTOOLS=true
poetry config http-basic.gitlab-backend-shared gitlab-ci-token -- "$GITLAB_READ_TOKEN"
poetry install --all-extras --sync --no-root
# shellcheck source=/dev/null
source .venv/bin/activate
pip install "poetry-dynamic-versioning[plugin]"
pre-commit install --install-hooks
gitlint install-hook
echo -e "${GREEN}Nix environment set up successfully!${NC}"
