# To learn more about how to use Nix to configure your environment
# see: https://developers.google.com/idx/guides/customize-idx-env
{ pkgs, ... }: {
  # Which nixpkgs channel to use. See https://channels.nixos.org/.
  channel = "stable-24.05"; # or "unstable"

  # Use https://search.nixos.org/packages to find packages
  # For signed commits: pkgs.gnupg pkgs.openssh
  # Note: we exclude Python, as we manage Python dependencies through uv, see docs/uv.md
  packages = [
    pkgs.gnupg
    pkgs.openssh
    pkgs.git-lfs
    pkgs.python312
    pkgs.docker
    pkgs.docker-compose
    pkgs.poetry
    pkgs.neovim
    pkgs.pre-commit
    pkgs.gitlint
    pkgs.mongodb-tools
  ];

  # Add common services https://developers.google.com/idx/guides/customize-idx-env#common-services
  services.docker.enable = true;
  services.mongodb.enable = true;
  services.redis.enable = true;

  # Sets environment variables in the workspace
  env = { };

  idx = {
    # Search for the extensions you want on https://open-vsx.org/ and use "publisher.id"
    extensions = [
      "vscodevim.vim"
      "charliermarsh.ruff"
      "ms-azuretools.vscode-docker"
      "ms-python.debugpy"
      "ms-python.python"
      "ritwickdey.LiveServer"
      "GitLab.gitlab-workflow"
      "rangav.vscode-thunder-client"
      "redhat.vscode-yaml"
      "ms-toolsai.jupyter"
    ];

    # Workspace lifecycle hooks
    workspace = {
      onCreate = {
        # Install latest uv tool because it does not yet exist on nixos.org/packages
        install-uv = ''
          if ! command -v uv &> /dev/null; then
              curl -LsSf https://astral.sh/uv/install.sh | sh
              export PATH="/home/user/.cargo/bin:$PATH"
          fi
        '';

        # Open editors for the following files by default, if they exist:
        default.openFiles = [ "README.md" ];
      };
      onStart = {
        git-log-alias = ''git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --branches"'';
      };
    };
  };
}
