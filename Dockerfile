FROM python:3.11.8-slim-bookworm

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# Install dependencies that can not be installed via Pip (e.g. Linux packages)
# and are required only by a repository that runs this script.
COPY --link *install_extra_dependencies.sh ./
RUN if [ -f install_extra_dependencies.sh ]; then \
        chmod +x install_extra_dependencies.sh && \
        ./install_extra_dependencies.sh && \
        rm install_extra_dependencies.sh; \
    fi

COPY ./app /code/app

# CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
CMD ["python",  "-m", "app.simplejob"]