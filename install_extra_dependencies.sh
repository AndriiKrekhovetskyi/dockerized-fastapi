# #!/bin/bash
# set -e

# # Install playwright
# python -m pip install playwright

# # Install playwright dependencies
# python -m playwright install firefox --with-deps
