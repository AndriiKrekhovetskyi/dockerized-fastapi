workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

default:
  interruptible: true
  timeout: 30 minutes
  tags:
    - own-group-runner
  retry:
    max: 1
    when: runner_system_failure

variables:
  FORCE_COLOR: 1
  # Git
  GIT_STRATEGY: fetch
  GIT_DEPTH: 1 # https://docs.gitlab.com/ee/ci/runners/configure_runners.html#shallow-cloning
  GIT_FETCH_EXTRA_FLAGS: --prune --quiet --no-tags  # https://docs.gitlab.com/ee/user/project/repository/monorepos/#git-fetch-extra-flags

stages:
  - renovate
  - lint
  - build

renovate:
  stage: renovate
  image:
    name: renovate/renovate:latest
    entrypoint: [""]
  script:
    - renovate

mega-linter:
  stage: lint
  allow_failure: true
  image: oxsecurity/megalinter-python:v7.13.0
  tags:
    - gitlab-org-docker
  script: ["/bin/bash /entrypoint.sh"]
  variables:
    DEFAULT_WORKSPACE: $CI_PROJECT_DIR
  artifacts:
    when: always
    paths:
      - megalinter-reports
    expire_in: 1 week
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes:
        compare_to: "refs/heads/main"
        paths:
          - "**/*"
          - "*"
    - if: $CI_COMMIT_BEFORE_SHA == "0000000000000000000000000000000000000000"
      when: never

docker_images:build:
  stage: build
  tags:
    - gitlab-org-docker
  image: docker:27.2.1
  services:
    - docker:27.2.1-dind
  script:
    - docker login -u $DOCKER_USERNAME -p $DOCKER_TOKEN
    - apk add --no-cache bash
    - bash -x $CI_PROJECT_DIR/ci_scripts/build_base_docker_images.sh
  rules:
    # Run after merge into main.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
    - when: never

python_packages:build:
  stage: build
  image: devcodeavors/poetry:1172565748
  script:
    # To keep things simple, we store all Python packages in a single Package Registry.
    - export SHARED_REPOSITORY_ID=39468326
    - poetry config repositories.gitlab-backend-shared "${CI_API_V4_URL}"/projects/$SHARED_REPOSITORY_ID/packages/pypi
    - poetry config http-basic.gitlab-backend-shared gitlab-ci-token -- "$CI_JOB_TOKEN"
    # For debugging
    - poetry --version && poetry config --list
    - cd $CI_PROJECT_DIR/src/python/dev_all
    - poetry build --format wheel -vv
    # We do not check if a version was published before, so we use `--skip-existing` to suppress errors.
    - poetry publish -vv --repository gitlab-backend-shared --skip-existing
    # Temporary publishing `asgi-idempotency-header` package for debugging purposes.
    - cd $CI_PROJECT_DIR/src/python/asgi-idempotency-header
    - poetry build --format wheel -vv
    # We do not check if a version was published before, so we use `--skip-existing` to suppress errors.
    - poetry publish -vv --repository gitlab-backend-shared --skip-existing
  rules:
    # Run after merge into main.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
    - when: never
